# TheMovieDb App framework
Example Movie app that uses the [TheMovieDatabase API](https://developers.themoviedb.org/3/getting-started) to get and post movie information.

### Resources
More info:
- [Guide to app Architecture](https://developer.android.com/jetpack/guide)
- [Retrofit](https://square.github.io/retrofit/)
- [Gson](https://github.com/google/gson#gson)
- [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
- [Room](https://developer.android.com/training/data-storage/room)
- [A Room with a View tutorial](https://developer.android.com/codelabs/android-room-with-a-view)
