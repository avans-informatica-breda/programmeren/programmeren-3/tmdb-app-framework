package nl.avans.movieapp.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class  BaseMovieAppController {

    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String BASE_POSTER_PATH_URL = "https://image.tmdb.org/t/p/w500";
    public static final String API_KEY = "863618e1d5c5f5cc4e34a37c49b8338e";
    public static final String SESSION_ID = "7459a7339eef7f0b73dde97f24474cfee87a9d44";

    protected final Retrofit retrofit;
    protected final Gson gson;

    public BaseMovieAppController() {
        gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

    }
}
